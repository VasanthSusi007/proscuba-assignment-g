library proscuba;

import 'package:flutter/material.dart';

const String mobileLabelText = 'Mobile Number';
const String passwordLabelText = 'Password';
const String reTypePasswordLabelText = 'Retype Password';
const String mobileHintText = 'enter your mobile number';
const String mobileCannotBeBlank = 'Mobile number cannot be blank';
const String invalidMobile = 'Invalid Mobile Number';
const String passwordHelperText = 'Remember your password is at least 8 chars';
const String passwordHintText = 'enter your password';
const String proscuba = 'Proscuba Marine';
const String login = 'login';
const String signup = 'signup';
const String forgotPassword = "Forgot Password ?";
const String orLoginUsing = "or Login Using";
const String facebook = "Facebook";
const String google = "Google";
const String nameHintText = "enter your name";
const String nameLabel = 'Name';
const String nameNotEmpty = "Name cannot be empty";
const String agree = "Agree to Terms and Conditions";
String getFormattedPhoneNumber(String _phoneNumber) {
  if (_phoneNumber.isEmpty) {
    return "";
  }

  String phoneNumber = _phoneNumber;
//  bool addPlus = phoneNumber.startsWith("1");
//  if (addPlus) phoneNumber = phoneNumber.substring(1);
  bool addParents = phoneNumber.length >= 3;
  bool addDash = phoneNumber.length >= 8;

  // +1
  String updatedNumber = "";
//  if (addPlus) updatedNumber += "+1";

  // (222)
  if (addParents) {
    updatedNumber += "";
    updatedNumber += phoneNumber.substring(0, 3);
    updatedNumber += "-";
  } else {
    updatedNumber += phoneNumber.substring(0);
    return updatedNumber;
  }

  // 111
  if (addDash) {
    updatedNumber += phoneNumber.substring(3, 6);
    updatedNumber += "-";
  } else {
    updatedNumber += phoneNumber.substring(3);
    return updatedNumber;
  }

  // 3333
  updatedNumber += phoneNumber.substring(6);
  return updatedNumber;
}