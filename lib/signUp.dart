import 'package:flutter/material.dart';
import 'package:proscuba/reusable_widgets.dart';
import 'package:proscuba/constants.dart' as global;
class SignUp extends StatefulWidget
{
  @override
  SignUpState createState() => SignUpState();
}

class SignUpState extends State<SignUp>
{
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController mobileController = new TextEditingController();
  bool _obscureText = true;
  String _mobile;
  String _name;
  String _password;
  bool valuefirst = false;
  String radioButtonItem = 'ONE';
  int id = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Image.asset('images/proscuba1.jpg',
                fit: BoxFit.fill,),
            ),
            Positioned(
              bottom: 0.0,
              width: MediaQuery.of(context).size.width,
              child: Container(
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0)),
                    color: Colors.white
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                       Header(global.signup),
                       nameField(),
                       mobileField(),
                       SizedBox(height: 20.0,),
                       optionsField(),
                       passwordField(),
                       checkBoxField(),
                       SizedBox(height: 10.0,),
                       signUpButton(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget nameField()
  {
    return TextFormField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.grey, height: 2),
          labelStyle: TextStyle(color: Colors.black),
          contentPadding: EdgeInsets.fromLTRB(0, 20.0, 0, 7),
          labelText: global.nameLabel,
          icon: Icon(Icons.person),
          hintText: global.nameHintText),
      validator: (value) => value.isEmpty
          ? global.nameNotEmpty
          : null,
      onSaved: (value) => _name = value,
    );
  }
  Widget mobileField() {
    return TextFormField(
      controller: mobileController,
      keyboardType: TextInputType.phone,
      maxLength: 12,
      decoration: InputDecoration(
          counterText: "",
          hintStyle: TextStyle(color: Colors.grey, height: 2),
          labelStyle: TextStyle(color: Colors.black),
          contentPadding: EdgeInsets.fromLTRB(0, 20.0, 0, 7),
          labelText: global.mobileLabelText,
          icon: Icon(Icons.phone_android),
          hintText: global.mobileHintText),
      validator: (value) => value.isEmpty
          ? global.mobileCannotBeBlank
          : value.length<10
          ? global.invalidMobile
          : null,
      onSaved: (value) => _mobile = value,
      onChanged: (value) => validateUsMobileNumber(value, mobileController),
    );
  }

  Widget passwordField() {
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      child: TextFormField(
        obscureText: _obscureText,
        decoration: InputDecoration(
            icon: Icon(Icons.lock),
            suffixIcon: IconButton(
              padding: EdgeInsets.fromLTRB(30, 40, 0, 0),
              icon: Icon(
                _obscureText ? Icons.visibility : Icons.visibility_off,
                size: 18.0,
                color: Colors.grey,
              ),
              onPressed: () {
                setState(() {
                  _obscureText = !_obscureText;
                });
              },
            ),
            enabledBorder: new UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.black, width: 0.8, style: BorderStyle.solid),
            ),
            labelText: global.passwordLabelText,
            contentPadding: EdgeInsets.fromLTRB(0, 20.0, 0, 7),
            labelStyle: TextStyle(color: Colors.black),
            helperText: global.passwordHelperText,
            helperStyle: TextStyle(color: Colors.grey),
            hintStyle: TextStyle(color: Colors.grey, height: 2),
            hintText: global.passwordHintText),
        validator: (value) => value.isEmpty || value.length < 8
            ? global.passwordHelperText
            : null,
        onSaved: (value) => _password = value,
      ),
    );
  }

  Widget signUpButton()
  {
    return Column(
      children: [
        MaterialButton(
          minWidth: double.infinity,
          color: Colors.blue,
          child: Text("Signup", style: TextStyle(color: Colors.white),),
          onPressed: ()
          {
           if(!this.valuefirst)
             {
               _scaffoldKey.currentState.showSnackBar(SnackBar(
                 content: Text("Please agree terms and conditions"),
               ));
             }
           else
           validateSignUp();
          },
        ),
        SizedBox(height: 10.0,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("already you have account? "),
            InkWell(child: Text("Login", style: TextStyle(color: Colors.blue),),
            onTap: ()
              {
                Navigator.of(context).pop();
              },)
          ],
        ),
        SizedBox(height: 10.0,),
      ],
    );
  }

  Widget checkBoxField()
  {
    return Row(
      children: [
        Checkbox(
          checkColor: Colors.greenAccent,
          activeColor: Colors.blue,
          value: this.valuefirst,
          onChanged: (bool value) {
            setState(() {
              this.valuefirst = value;
            });
          },
        ),
        Text(global.agree)
      ],
    );
  }

  Widget optionsField()
  {
    return Row(
      children: [
        Icon(Icons.person_add_alt_1_outlined, color: Colors.grey,),
        Container(
          child: Row(
            children: [
              Radio(
                value: 1,
                groupValue: id,
                onChanged: (val) {
                  setState(() {
                    radioButtonItem = 'ONE';
                    id = 1;
                  });
                },
              ),
              Text(
                'Driver',
              ),

              Radio(
                value: 2,
                groupValue: id,
                onChanged: (val) {
                  setState(() {
                    radioButtonItem = 'TWO';
                    id = 2;
                  });
                },
              ),
              Text(
                'Professional',
              ),

              Radio(
                value: 3,
                groupValue: id,
                onChanged: (val) {
                  setState(() {
                    radioButtonItem = 'THREE';
                    id = 3;
                  });
                },
              ),
              Text(
                'Operator',
              ),
            ],
          ),
        ),
      ],
    );
  }
  void validateUsMobileNumber(
      String phoneNumber, TextEditingController textField) {
    if (phoneNumber.contains("\(") && !phoneNumber.contains("\)")) {
      phoneNumber = phoneNumber.substring(0, phoneNumber.length - 1);
    }
    phoneNumber = phoneNumber.replaceAll(RegExp("[^0-9]"), "");
    int maxLength = 10;
    if (phoneNumber.length == 10) {
      phoneNumber = phoneNumber.substring(0, maxLength);
      String usMobileNoFormat = global.getFormattedPhoneNumber(phoneNumber);
      setState(() {
        FocusScope.of(context).nextFocus();
        textField.text = usMobileNoFormat;
      });
    }
  }

  void validateSignUp()
  {
    final form = _formKey.currentState;
    if(form.validate())
      {
        form.save();
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Successfully Registered")));
      }else{
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Please Enter Valid Details"),
      ));
    }
  }
}