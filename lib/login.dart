
import 'package:proscuba/constants.dart' as global;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proscuba/reusable_widgets.dart';
import 'package:proscuba/signUp.dart';

class Login extends StatefulWidget
{
  @override
  LoginState createState()=> LoginState();
}

class LoginState extends State<Login>
{
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Map<String, String> _account;
  bool _obscureText = true;
  String _mobile;
  String _password;
  TextEditingController mobileController = new TextEditingController();

  @override
  void initState()
  {
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Image.asset('images/proscuba.jpg',
                 fit: BoxFit.fill,),
            ),
            Positioned(
              bottom: 0.0,
              width: MediaQuery.of(context).size.width,
              child: Container(
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0),
                     topRight: Radius.circular(20.0)),
                    color: Colors.white
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Header(global.login),
                      mobileField(),
                      passwordField(),
                      forgotPassword(),
                      loginButton(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget mobileField() {
    return TextFormField(
      controller: mobileController,
      keyboardType: TextInputType.phone,
      maxLength: 12,
      decoration: InputDecoration(
        counterText: "",
          hintStyle: TextStyle(color: Colors.grey, height: 2),
          labelStyle: TextStyle(color: Colors.black),
          contentPadding: EdgeInsets.fromLTRB(0, 20.0, 0, 7),
          labelText: global.mobileLabelText,
          icon: Icon(Icons.phone_android),
          hintText: global.mobileHintText),
      validator: (value) => value.isEmpty
          ? global.mobileCannotBeBlank
          : value.length<10
          ? global.invalidMobile
          : null,
      onChanged: (value)=> validateUsMobileNumber( value, mobileController),
      onSaved: (value) => _mobile = value,
    );
  }

  Widget passwordField() {
    return Container(
      margin: EdgeInsets.only(bottom: 20.0),
      child: TextFormField(
        obscureText: _obscureText,
        decoration: InputDecoration(
          icon: Icon(Icons.lock),
            suffixIcon: IconButton(
              padding: EdgeInsets.fromLTRB(30, 40, 0, 0),
              icon: Icon(
                _obscureText ? Icons.visibility : Icons.visibility_off,
                size: 18.0,
                color: Colors.grey,
              ),
              onPressed: () {
                setState(() {
                  _obscureText = !_obscureText;
                });
              },
            ),
            enabledBorder: new UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.black, width: 0.8, style: BorderStyle.solid),
            ),
            labelText: global.passwordLabelText,
            contentPadding: EdgeInsets.fromLTRB(0, 20.0, 0, 7),
            labelStyle: TextStyle(color: Colors.black),
            helperText: global.passwordHelperText,
            helperStyle: TextStyle(color: Colors.grey),
            hintStyle: TextStyle(color: Colors.grey, height: 2),
            hintText: global.passwordHintText),
        validator: (value) => value.isEmpty || value.length < 8
            ? global.passwordHelperText
            : null,
        onSaved: (value) => _password = value,
      ),
    );
  }
   Widget forgotPassword() 
  {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        InkWell(child: Text(global.forgotPassword,),
        onTap: ()
          {

          },),
      ],
    );
  }

   Widget loginButton()
  {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: Column(
        children: [
          MaterialButton(
            minWidth: double.infinity,
            color: Colors.blue,
            child: Text(global.login, style: TextStyle(color: Colors.white),),
            onPressed: ()
            {
              validate();
            },
          ),
          SizedBox(height: 10.0,),
          Text(global.orLoginUsing),
          SizedBox(height: 10.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MaterialButton(
                minWidth: 150,
                color: Colors.indigo,
                child: Row(
                  children: [
                    Image.asset('images/facebook.png',
                     width: 20.0,
                    height: 20.0,),
                    SizedBox(width: 10.0,),
                    Text(global.facebook, style: TextStyle(color: Colors.white),),
                  ],
                ),
                onPressed: ()
                {

                },
              ),
              MaterialButton(
                color: Colors.white,
                minWidth: 150,
                child: Row(
                  children: [
                    Image.asset('images/google.jpg',
                      width: 20.0,
                      height: 20.0,),
                    SizedBox(width: 10.0,),
                    Text(global.google, style: TextStyle(color: Colors.black),),
                  ],
                ),
                onPressed: ()
                {

                },
              ),
            ],
          ),
          SizedBox(height: 20.0,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Don't have an account? ",style: TextStyle(color: Colors.grey),),
              InkWell(child: Text("Sign Up", style: TextStyle(color: Colors.blue),),
              onTap: (){
               Navigator.push(context, CupertinoPageRoute(builder: (context)=>
                SignUp()));
              },),
            ],
          ),
          SizedBox(height: 20.0,),
        ],
      ),
    );
  }

  void validate()
  {
    final form = _formKey.currentState;
     if(form.validate())
       {
         form.save();
         _scaffoldKey.currentState.showSnackBar(SnackBar(
           content: Text("Login Success"),
         ));
       }
     else
       _scaffoldKey.currentState.showSnackBar(SnackBar(
         content: Text("Please Enter Valid Details"),
       ));
  }

  void validateUsMobileNumber(
      String phoneNumber, TextEditingController textField) {
    if (phoneNumber.contains("\(") && !phoneNumber.contains("\)")) {
      phoneNumber = phoneNumber.substring(0, phoneNumber.length - 1);
    }
    phoneNumber = phoneNumber.replaceAll(RegExp("[^0-9]"), "");
    int maxLength = 10;
    if (phoneNumber.length == 10) {
      phoneNumber = phoneNumber.substring(0, maxLength);
      String usMobileNoFormat = global.getFormattedPhoneNumber(phoneNumber);
      setState(() {
        FocusScope.of(context).nextFocus();
        textField.text = usMobileNoFormat;
      });
    }
  }
}