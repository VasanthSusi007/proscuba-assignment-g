import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proscuba/constants.dart' as global;
class Header extends StatelessWidget
{
  String subTitle;
  Header(this.subTitle):super();
  @override
  Widget build(BuildContext context)
  {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 10.0,),
          Text(global.proscuba,style: TextStyle(
              color: Colors.blue,
              fontSize: 25.0,
              fontWeight: FontWeight.bold
          ),),
          SizedBox(height: 10.0,),
          Text(subTitle.toUpperCase(),style: TextStyle(
            color: Colors.black,
            fontSize: 18.0,
            fontWeight: FontWeight.bold
          ),),
        ],
      ),
    );
  }
}