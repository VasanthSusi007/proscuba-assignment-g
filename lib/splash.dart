import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proscuba/login.dart';
import 'package:proscuba/constants.dart' as global;

class Splashscreen extends StatefulWidget {
  @override
  _SplashscreenState createState() => _SplashscreenState();
}

class _SplashscreenState extends State<Splashscreen> with SingleTickerProviderStateMixin{
  AnimationController controller;

  Animation animation;

  double beginAnim = 0.0 ;
  double endAnim = 1.0 ;
  @override
  void initState(){
    super.initState();
    controller = AnimationController(
        duration: const Duration(seconds: 5), vsync: this);
    animation = Tween(begin: beginAnim, end: endAnim).animate(controller)
      ..addListener(() {
        setState(() {

        });
      });
    startProgress();
  }
  @override
  void dispose() {
    controller.stop();
    super.dispose();
  }

  startProgress(){
    controller.forward();
    Timer(Duration(seconds: 6), (){
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => Login(),
      ));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:
          Stack(
            children: [
              Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0))
                    ),
                    child: Image.asset('images/proscuba1.jpg',
                    fit: BoxFit.fill,),
                  ),
              Positioned(
                bottom: 0.0,
                width: MediaQuery.of(context).size.width,
                child: Container(
                  height: 250,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(global.proscuba,style: TextStyle(
                            color: Colors.blue,
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold
                        ),),
                      Container(
                       padding: EdgeInsets.all(20.0),
                       child: Column(
                         children: [
                           Text("Loading",style: TextStyle(fontWeight: FontWeight.bold,
                           fontSize: 16.0),),
                           SizedBox(height: 15.0,),
                           LinearProgressIndicator(
                             minHeight: 5.0,
                             backgroundColor: Colors.grey,
                             value: animation.value,
                           ),
                         ],
                       )),
                    ],
                  ),

                ),
              )
            ],
          ),
    );
  }
}